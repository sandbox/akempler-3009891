
var botmanWidget = {
  chatServer: '/botman/' + drupalSettings.botman.botman_route,
  frameEndpoint: '/botman/widget',
  title: drupalSettings.botman.header_title,
  aboutText: drupalSettings.botman.about_text,
  aboutLink: drupalSettings.botman.about_link,
  bubbleAvatarUrl: 'https://botman.io/img/logo.png',
  introMessage: drupalSettings.botman.intro_message,
  mainColor: '#' + drupalSettings.botman.header_bg_color,
  headerTextColor: '#' + drupalSettings.botman.header_text_color,
  bubbleBackground: '#' + drupalSettings.botman.widget_launcher_bg_color,
  timeFormat: 'h:mm'
};
